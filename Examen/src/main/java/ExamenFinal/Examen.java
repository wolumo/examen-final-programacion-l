
package ExamenFinal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class Examen {

    
    public static void main(String[] args) {   
        Map<String, Integer> m = new HashMap<>();
        List <String> lista = new ArrayList();
        List <String> borra = new ArrayList();
        conecciones f = new conecciones();
        String t = "";
        int i,j;
        try {
            System.out.println("Leyendo el archivo del examen");
            t = f.llenarString();     
            String[] parcial = t.split(" ");
            borra = freq(parcial);
            for(String par : parcial){
            lista.add(par);  
            printlist(lista);
            
        }  
            
            
            
        } catch (IOException ex) {
            Logger.getLogger(Examen.class.getName()).log(Level.SEVERE, null, ex);
  
        
    }    
        
    }
    public static void printlist(List <String> lista){
        for(String elemento: lista){
            System.out.print(elemento+" ");
        }
    }
    
    public static List<String> freq(String[] txt) throws IOException{
       List <String> b = new ArrayList<>();
       conecciones f = new conecciones();
       Map<String, Integer> m = new HashMap<>();
       for (String t : txt){
           Integer frecuencia = m.get(t);
           m.put(t,(frecuencia==null)?1:frecuencia+1);
           if(m.get(t)==1){
               b.add(t);
           }
       }
       
       System.out.println("Contiene las siguientes palabras: \n");
       for(String ta : b){
           System.out.print(ta+" ");
       }

       System.out.println("\n Palabras ordenadas dependiendo su frecuencia: ");
       m = Ordenadas(m);
       
       
       Object[] sa = m.entrySet().toArray();
       f.crear(sa); System.out.print("Creando en el arcvhivo Examen_ArchivoFinal :"+m);
       
       return b;
    }
   
        public static Map<String, Integer> Ordenadas(Map<String, Integer> ordenar) {
        ordenar = ordenar.entrySet().stream().sorted((Map.Entry.<String, Integer>comparingByValue().reversed())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        
        return ordenar;
}
}

